const TestLinkApi = require('testlink-api-client')
const http = require('http')

const updateResultsParams = {
  devKey: 'The TestLink dev key, which can be generated in TestLink:\n' +
          'TestLink -> My Settings -> Personal API access key -> Generate a new key',
  apiUrl: 'Looks something like this: http://IP_ADDR:80/lib/api/xmlrpc/v1/xmlrpc.php',
  project: 'The test project',
  plan: 'The test plan',
  build: 'The test build',
  suite: 'The test suite',
  total: 'Total number of tests',
  passed: 'Number of tests considered passed',
  failed: 'Number of tests considered failed',
  blocked: 'Number of tests considered blocked'
}

// devKey
// TestLink -> My Settings -> Personal API access key -> Generate a new key
// http://chaitanyaqa.github.io/testlink-api-client/classes/TestLinkApi.html#methods

const getProjectId = (testLink, name) => new Promise((resolve, reject) => {
  testLink.getProjects(projects => {
    resolve(projects.find(project => project.struct.name === name).struct.id)
  })
})

const getTestPlanId = (testLink, testprojectid, name) => new Promise((resolve, reject) => {
  testLink.getProjectTestPlans({ testprojectid }, testPlans => {
    resolve(testPlans.find(plan => plan.struct.name === name).struct.id)
  })
})

const getBuildId = (testLink, testplanid, name) => new Promise((resolve, reject) => {
  testLink.getBuildsForTestPlan({ testplanid }, _builds => {
    const builds = (_builds instanceof Array) ? _builds : [_builds]
    resolve(builds.find(build => build.struct.name === name).struct.id)
  })
})

// Not actually necessary for anything
const getTestSuiteId = (testLink, testplanid, name) => new Promise((resolve, reject) => {
  testLink.getTestSuitesForTestPlan({ testplanid }, suites => {
    resolve(suites.find(suite => suite.struct.name === name).struct.id)
  })
})

const getTestCaseIdsBySuite = (testLink, testprojectid, testsuiteid) => new Promise((resolve, reject) => {
  testLink.getTestCasesForTestSuite({ testprojectid, testsuiteid }, testCases => {
    resolve(
      testCases
      // TestLink has a cool feature where tests deleted from the suite queried
      // are returned from getTestCasesForTestSuite
      .filter(testCase => testCase.struct.parent_id === testsuiteid)
      .map(testCase => testCase.struct.id)
    )
  })
})

const updateStatus = (testLink, testplanid, platform, testcaseid, buildid, status) => new Promise((resolve, reject) => {
  // The testlink-api-client code has a bug where this particular request fails
  // Now I could do a merge request and fix the code, but I want TestLink to die
  // So here's a lazy workaround.

  // Much of this code is more or less copied/pasted from the project

  const notes = 'TEST EXECUTED VIA TECHNOLOGY'
  const user = 'jesweene'
  const bugid = 'N/A'
  const requestBody = `
    <?xml version="1.0"?>
    <methodCall>
    <methodName>tl.reportTCResult</methodName>
    <params><param><value><struct>
    <member><name>devKey</name><value><string>${testLink.devkey}</string></value></member>
    <member><name>testplanid</name><value><int>${testplanid}</int></value></member>
    <member><name>testcaseid</name><value><string>${testcaseid}</string></value></member>
    <member><name>buildid</name><value><string>${buildid}</string></value></member>
    <member><name>notes</name><value><string>${notes}</string></value></member>
    <member><name>platformname</name><value><string>${platform}</string></value></member>
    <member><name>user</name><value><string>${user}</string></value></member>
    <member><name>bugid</name><value><string>${bugid}</string></value></member>
    <member><name>overwrite</name><value><boolean>overwrite</boolean></value></member>
    <member><name>status</name><value><string>${status}</string></value></member>
    </struct></value></param>
    </params></methodCall>
  `
  const tokens = testLink.url.match(/http:\/\/([^:]*):([^/]*)([^$]*)/)
  const [ url, host, port, path ] = tokens // eslint-disable-line no-unused-vars

  const req = http.request({
    host,
    path,
    port,
    method: 'POST',
    headers: { Cookie: 'cookie', 'Content-Type': 'text/xml' }
  }, res => {
    let buffer = ''
    res.on('data', data => {
      buffer = buffer + data
    })
    res.on('end', data => {
      resolve(buffer)
    })
  })
  req.write(requestBody)
  req.end()
})

module.exports = Object.freeze({
  generateTestSuite (_config) {
    const config = Object.assign({
      name: '',
      details: 'Output of some tool used for reporting test results in TestLink',
      summary: 'A blank test representing a percentage of the total tests run'
    }, _config || {})
    const { name, details, summary } = config
    if (!name) {
      throw new Error('Please specify a unique "name" for the Test Suite!')
    }
    let output = [
      `<testsuite name="${name}">`,
      `  <details>${details}</details>`
    ]
    for (let i = 1; i <= 100; i++) {
      output.push('  ' +
        `<testcase name="Test ${i}">` +
        `<summary>${summary}</summary>` +
        `<executionType>1</executionType>` +
        `<importance>2</importance>` +
        `<status>1</status>` +
        `</testcase>`
      )
    }
    output.push(`</testsuite>`)
    return output.join('\n')
  },

  updateResults (config) {
    const missingParams = []
    Object.entries(updateResultsParams).forEach(([field, description]) => {
      if (config[field] == null) {
        missingParams.push(`"${field}" (${description})`)
      }
    })
    if (missingParams.length) {
      throw new Error(
        'The following fields were missing from config:\n' +
        missingParams.join('\n')
      )
    }

    const {
      devKey, apiUrl,
      project, plan, build, suite,
      total, passed, failed, blocked
    } = config
    const platform = config.platform || 'SAMPLE PLATFORM'

    // Failed/Blocked percents are never less than 1%
    // Percents always round up
    // Passed percent does not round up unless all tests are actually passed
    const blockedPercent = !blocked ? 0
    : Math.ceil(blocked * 100 / total)
    const failedPercent = !failed ? 0
    : Math.min(
        100 - blockedPercent,
        Math.ceil(failed * 100 / total)
      )
    const passedPercent = !passed ? 0
    : Math.min(
        100 - blockedPercent - failedPercent,
        (passed === total) ? 100 : 99,
        Math.ceil(passed * 100 / total)
      )

    // console.log(
    //   `${passedPercent} passed, ${failedPercent} failed, ${blockedPercent} blocked`
    // )
    // return

    const testLink = new TestLinkApi(devKey, apiUrl)
    return getProjectId(testLink, project)
    .then(projectId =>
      getTestPlanId(testLink, projectId, plan)
      .then(testPlanId => {
        return Promise.all([
          getBuildId(testLink, testPlanId, build),
          getTestSuiteId(testLink, testPlanId, suite)
          .then(testSuiteId => getTestCaseIdsBySuite(testLink, projectId, testSuiteId))
        ])
        .then(([buildId, testCaseIds]) => {
          const statusUpdatePromises = []
          const updateStat = (index, status) =>
            updateStatus(
              testLink,
              testPlanId,
              platform,
              testCaseIds[index],
              buildId,
              status
            )
          for (let passedAmt = 0; passedAmt < passedPercent; passedAmt++) {
            statusUpdatePromises.push(updateStat(passedAmt, 'p'))
          }
          for (let failedAmt = 0; failedAmt < failedPercent; failedAmt++) {
            statusUpdatePromises.push(updateStat(passedPercent + failedAmt, 'f'))
          }
          for (let blockedAmt = 0; blockedAmt < blockedPercent; blockedAmt++) {
            statusUpdatePromises.push(updateStat(passedPercent + failedPercent + blockedAmt, 'b'))
          }

          return Promise.all(statusUpdatePromises)
        })
      })
    )
  }
})
